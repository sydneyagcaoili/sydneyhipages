package au.com.hipages.data.utils

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext


@Throws(InterruptedException::class)
actual fun <T> runBlocking(context: CoroutineContext, block: suspend CoroutineScope.() -> T): T {
        return kotlinx.coroutines.runBlocking(context, block)
}
