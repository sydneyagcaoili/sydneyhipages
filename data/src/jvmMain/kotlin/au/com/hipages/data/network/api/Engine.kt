package au.com.hipages.data.network.api

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.android.Android

actual val httpClientEngine : HttpClientEngine = Android.create()