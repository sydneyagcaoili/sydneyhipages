package au.com.hipages.data.network.models

import au.com.hipages.data.utils.DateSerializer
import au.com.hipages.domain.models.Date
import kotlinx.serialization.Serializable

@Serializable
data class NJob(
    val jobId: Long,
    val category: String,
    @Serializable(with= DateSerializer::class) val postedDate: Date,
    val status: String,
    val connectedBusinesses: List<NBusiness>?
) {

}