package au.com.hipages.data.network.models

import kotlinx.serialization.Serializable

@Serializable
data class NBusiness(
    val businessId: Long,
    val thumbnail: String,
    val isHired: Boolean
) {

}