package au.com.hipages.data.network.models

import kotlinx.serialization.Serializable

@Serializable
data class JobResponse(val jobs : List<NJob>){}
