package au.com.hipages.data.network.api

import io.ktor.client.engine.HttpClientEngine

expect val httpClientEngine : HttpClientEngine