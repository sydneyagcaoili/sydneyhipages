package au.com.hipages.data.network.api

import io.ktor.http.ContentType

object ContentType {

    object Application{
        val JsonApi = ContentType("application", "json")
    }
}
