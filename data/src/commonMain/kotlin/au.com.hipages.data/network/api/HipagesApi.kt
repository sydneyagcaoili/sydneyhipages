package au.com.hipages.data.network.api

import au.com.hipages.data.network.api.ContentType.Application.JsonApi
import au.com.hipages.data.network.models.JobResponse
import au.com.hipages.data.network.models.NBusiness
import au.com.hipages.data.network.models.NJob
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.request.url
import io.ktor.http.Url
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.modules.SerializersModule

@UnstableDefault
val hiPagesJson = Json(
    context = SerializersModule {

    },
    configuration = JsonConfiguration(
        strictMode = false,
        encodeDefaults = false
    )
)

class HipagesApi(val engine: HttpClientEngine, val baseUrl: String) {
    @UnstableDefault
    val serializer = KotlinxSerializer(hiPagesJson).apply {
        register(JobResponse.serializer())
        register(NJob.serializer())
        register(NBusiness.serializer())
    }


    private var v2Client = HttpClient(engine) {
        install(JsonFeature) {
            this.acceptContentTypes = listOf(JsonApi)
            serializer = this@HipagesApi.serializer
        }

    }

    //TODO apply real remote rest data serialization
    suspend fun getJobs() = GlobalScope.async {
            val response = v2Client.get<String> {
                url(Url("https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json"))
            }
            return@async Json.nonstrict.parse(JobResponse.serializer(), response)
        }.await()
}