package au.com.hipages.data.utils

import au.com.hipages.domain.models.Date
import com.soywiz.klock.DateFormat
import com.soywiz.klock.parse
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Serializer(forClass = Date::class)
object DateSerializer: KSerializer<Date> {
    val df = DateFormat("yyyy-MM-dd")

    override val descriptor: SerialDescriptor =
            StringDescriptor.withName("WithCustomDefault")

    override fun serialize(output: Encoder, obj: Date) {
        //output.encode(df.format(obj))
    }

    override fun deserialize(input: Decoder): Date {
        return Date(df.parse(input.decodeString()).local.unixMillisLong)
    }
}
