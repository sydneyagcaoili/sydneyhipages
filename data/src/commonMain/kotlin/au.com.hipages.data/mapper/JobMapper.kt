package au.com.hipages.data.mapper

import au.com.hipages.data.network.models.NJob
import au.com.hipages.domain.models.Job

class JobMapper() {
    var businessMapper = BusinessMapper()

    fun toDomainModel(nJob: NJob): Job {
        return Job(nJob.jobId,
            nJob.category,
            nJob.postedDate,
            Job.determineStatus(nJob.status),
            nJob.connectedBusinesses?.map { businessMapper.toDomainModel(it) })
    }
}