package au.com.hipages.data.mapper

import au.com.hipages.data.network.models.NBusiness
import au.com.hipages.data.network.models.NJob
import au.com.hipages.domain.models.Business
import au.com.hipages.domain.models.Job

class BusinessMapper () {

    fun toDomainModel(nBusiness : NBusiness) : Business {
        return Business(nBusiness.businessId, nBusiness.thumbnail, nBusiness.isHired)
    }
}