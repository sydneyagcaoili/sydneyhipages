package au.com.hipages.data.repositories

import au.com.hipages.data.network.api.HipagesApi
import au.com.hipages.data.network.api.httpClientEngine
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
open class DataRepository {
    //TODO build version
    val api = HipagesApi(httpClientEngine, "https://s3-ap-southeast-2.amazonaws.com/")

}