package au.com.hipages.data.repositories

import au.com.hipages.data.mapper.JobMapper
import au.com.hipages.domain.models.Job
import au.com.hipages.domain.repositories.JobRepository
import au.com.hipages.domain.utils.TDispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce

@ExperimentalCoroutinesApi
class JobDataRepository : DataRepository(), JobRepository {
    val jobMapper = JobMapper()
    //TODO APPLY SQLDELIGHT
    override suspend fun fetchOpenJobs(): ReceiveChannel<List<Job>> = GlobalScope.produce(TDispatchers.Default) {
        val njobs = api.getJobs()
        send(njobs.jobs.map {
            jobMapper.toDomainModel(it)
        }.filter {
            Job.Status.CLOSED != it.status
        })
    }

    override suspend fun fetchClosedJobs(): ReceiveChannel<List<Job>> = GlobalScope.produce(TDispatchers.Default) {
        val njobs = api.getJobs()
        send(njobs.jobs.map {
            jobMapper.toDomainModel(it)
        }.filter {
            Job.Status.CLOSED == it.status
        })
    }

    override suspend fun listenToOpenJobs(): BroadcastChannel<List<Job>> {
        return BroadcastChannel<List<Job>>(4)
    }

    override suspend fun listenToClosedJobs(): BroadcastChannel<List<Job>> {
        return BroadcastChannel<List<Job>>(4)
    }

}