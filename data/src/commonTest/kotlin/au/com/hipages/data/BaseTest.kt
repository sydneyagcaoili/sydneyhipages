package au.com.hipages.data

import au.com.hipages.data.network.api.HipagesApi
import au.com.hipages.data.network.api.httpClientEngine

open class BaseTest {
    val api = HipagesApi(httpClientEngine, "https://s3-ap-southeast-2.amazonaws.com/")
}