package au.com.hipages.data

import au.com.hipages.data.utils.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class JobsApiTest : BaseTest() {
    @Test
    fun testGetJobs() {
        runBlocking {
            val response = api.getJobs()
            assertNotNull(response)
            assertEquals(4, response.jobs.size)
        }
    }
}