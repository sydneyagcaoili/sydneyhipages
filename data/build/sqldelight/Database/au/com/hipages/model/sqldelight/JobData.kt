package au.com.hipages.model.sqldelight

import kotlin.Long
import kotlin.String

interface JobData {
    val id: Long

    val category: String

    val status: String

    data class Impl(
        override val id: Long,
        override val category: String,
        override val status: String
    ) : JobData {
        override fun toString(): String = """
        |JobData.Impl [
        |  id: $id
        |  category: $category
        |  status: $status
        |]
        """.trimMargin()
    }
}
