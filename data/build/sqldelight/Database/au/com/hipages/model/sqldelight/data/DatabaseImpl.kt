package au.com.hipages.model.sqldelight.data

import au.com.hipages.model.sqldelight.Database
import au.com.hipages.model.sqldelight.JobData
import au.com.hipages.model.sqldelight.JobDataQueries
import com.squareup.sqldelight.Query
import com.squareup.sqldelight.TransacterImpl
import com.squareup.sqldelight.db.SqlCursor
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.internal.copyOnWriteList
import kotlin.Any
import kotlin.Int
import kotlin.Long
import kotlin.String
import kotlin.collections.MutableList
import kotlin.reflect.KClass

internal val KClass<Database>.schema: SqlDriver.Schema
    get() = DatabaseImpl.Schema

internal fun KClass<Database>.newInstance(driver: SqlDriver): Database = DatabaseImpl(driver)

private class DatabaseImpl(driver: SqlDriver) : TransacterImpl(driver), Database {
    override val jobDataQueries: JobDataQueriesImpl = JobDataQueriesImpl(this, driver)

    object Schema : SqlDriver.Schema {
        override val version: Int
            get() = 1

        override fun create(driver: SqlDriver) {
            driver.execute(null, """
                    |CREATE TABLE JobData(
                    |    id INTEGER PRIMARY KEY,
                    |    category TEXT NOT NULL,
                    |    status TEXT NOT NULL
                    |)
                    """.trimMargin(), 0)
        }

        override fun migrate(
            driver: SqlDriver,
            oldVersion: Int,
            newVersion: Int
        ) {
        }
    }
}

private class JobDataQueriesImpl(private val database: DatabaseImpl, private val driver: SqlDriver)
        : TransacterImpl(driver), JobDataQueries {
    internal val getById: MutableList<Query<*>> = copyOnWriteList()

    internal val getAll: MutableList<Query<*>> = copyOnWriteList()

    internal val count: MutableList<Query<*>> = copyOnWriteList()

    override fun <T : Any> getById(id: Long, mapper: (
        id: Long,
        category: String,
        status: String
    ) -> T): Query<T> = GetById(id) { cursor ->
        mapper(
            cursor.getLong(0)!!,
            cursor.getString(1)!!,
            cursor.getString(2)!!
        )
    }

    override fun getById(id: Long): Query<JobData> = getById(id, JobData::Impl)

    override fun <T : Any> getAll(mapper: (
        id: Long,
        category: String,
        status: String
    ) -> T): Query<T> = Query(1, getAll, driver, """
    |SELECT *
    |FROM JobData
    """.trimMargin()) { cursor ->
        mapper(
            cursor.getLong(0)!!,
            cursor.getString(1)!!,
            cursor.getString(2)!!
        )
    }

    override fun getAll(): Query<JobData> = getAll(JobData::Impl)

    override fun count(): Query<Long> = Query(2, count, driver, """
    |SELECT COUNT(id)
    |FROM JobData
    """.trimMargin()) { cursor ->
        cursor.getLong(0)!!
    }

    override fun insert(
        id: Long?,
        category: String,
        status: String
    ) {
        driver.execute(3, """
        |INSERT OR REPLACE INTO JobData(id, category, status)
        |VALUES (?1, ?2, ?3)
        """.trimMargin(), 3) {
            bindLong(1, id)
            bindString(2, category)
            bindString(3, status)
        }
        notifyQueries(database.jobDataQueries.getById + database.jobDataQueries.getAll +
                database.jobDataQueries.count)
    }

    override fun deleteAll() {
        driver.execute(4, """DELETE FROM JobData""", 0)
        notifyQueries(database.jobDataQueries.getById + database.jobDataQueries.getAll +
                database.jobDataQueries.count)
    }

    override fun deleteById(id: Long) {
        driver.execute(5, """DELETE FROM JobData WHERE id = ?1""", 1) {
            bindLong(1, id)
        }
        notifyQueries(database.jobDataQueries.getById + database.jobDataQueries.getAll +
                database.jobDataQueries.count)
    }

    private inner class GetById<out T : Any>(private val id: Long, mapper: (SqlCursor) -> T) :
            Query<T>(getById, mapper) {
        override fun execute(): SqlCursor = driver.executeQuery(0, """
        |SELECT *
        |FROM JobData
        |WHERE id = ?1
        """.trimMargin(), 1) {
            bindLong(1, id)
        }
    }
}
