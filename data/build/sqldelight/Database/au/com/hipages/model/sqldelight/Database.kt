package au.com.hipages.model.sqldelight

import au.com.hipages.model.sqldelight.data.newInstance
import au.com.hipages.model.sqldelight.data.schema
import com.squareup.sqldelight.Transacter
import com.squareup.sqldelight.db.SqlDriver

interface Database : Transacter {
    val jobDataQueries: JobDataQueries

    companion object {
        val Schema: SqlDriver.Schema
            get() = Database::class.schema

        operator fun invoke(driver: SqlDriver): Database = Database::class.newInstance(driver)}
}
