package au.com.hipages.model.sqldelight

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import kotlin.Any
import kotlin.Long
import kotlin.String

interface JobDataQueries : Transacter {
    fun <T : Any> getById(id: Long, mapper: (
        id: Long,
        category: String,
        status: String
    ) -> T): Query<T>

    fun getById(id: Long): Query<JobData>

    fun <T : Any> getAll(mapper: (
        id: Long,
        category: String,
        status: String
    ) -> T): Query<T>

    fun getAll(): Query<JobData>

    fun count(): Query<Long>

    fun insert(
        id: Long?,
        category: String,
        status: String
    )

    fun deleteAll()

    fun deleteById(id: Long)
}
