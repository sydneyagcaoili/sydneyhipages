package au.com.hipages.bases

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import au.com.hipages.domain.bases.mvp.MvpPresenter
import au.com.hipages.domain.bases.mvp.MvpView

@Suppress("UNCHECKED_CAST")
abstract class MvpActivity<V : MvpView, P : MvpPresenter<V>> : AppCompatActivity(), MvpView {
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
        presenter.attachView(this as V)
    }

    abstract fun createPresenter() : P

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}
