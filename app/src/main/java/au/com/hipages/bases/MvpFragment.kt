package au.com.hipages.bases


import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import au.com.hipages.domain.bases.mvp.MvpPresenter
import au.com.hipages.domain.bases.mvp.MvpView


@Suppress("UNCHECKED_CAST")
abstract class MvpFragment<V : MvpView, P : MvpPresenter<V>>(@LayoutRes resId : Int) : ResFragment(resId), MvpView {
    lateinit var presenter: P
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this as V)
    }


    abstract fun createPresenter(): P
}
