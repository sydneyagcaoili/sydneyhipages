package au.com.hipages.features.jobs

import android.annotation.SuppressLint
import android.view.View
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import au.com.hipages.R
import au.com.hipages.domain.models.Job
import au.com.hipages.domain.models.toReadableDateString
import au.com.hipages.util.decoration.SpaceDecoration
import com.xiaofeng.flowlayoutmanager.Alignment
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import kotlinx.android.synthetic.main.item_job.view.*

@SuppressLint("InflateParams")
class JobItemViewHolder(
    itemView: View,
    var onCloseActionClicked: (job: Job) -> Unit?
) :
    RecyclerView.ViewHolder(itemView) {
    var businessThumbnailAdapter: BusinessThumbnailAdapter

    init {
        businessThumbnailAdapter = BusinessThumbnailAdapter()
        itemView.rvConnectedBusinesses.setHasFixedSize(false)
        itemView.rvConnectedBusinesses.layoutManager = FlowLayoutManager().apply {
            removeItemPerLineLimit()
            isAutoMeasureEnabled = true
            setAlignment(Alignment.CENTER)
        }
        itemView.rvConnectedBusinesses.addItemDecoration(
            SpaceDecoration(
                itemView.rvConnectedBusinesses.context, 8f, 0f, 8f, 0f
            )
        )
        itemView.rvConnectedBusinesses.adapter = businessThumbnailAdapter
    }

    fun bind(item: Job) {
        itemView.tvCategory.setText(item.category)
        itemView.tvStatus.setText(item.status.stringValue)
        itemView.tvStatus.setTextColor(
            ContextCompat.getColor(
                itemView.context,
                if (item.status == Job.Status.IN_PROGRESS) R.color.accentBlue else R.color.accentRed
            )
        )
        val postedDateRemark =
            "${itemView.context.getString(R.string.posted)}:${item.postedDate.toReadableDateString()}"
        itemView.tvPostedDate.setText(postedDateRemark)
        val remark: String
        if (item.connectedBusinesses != null && item.connectedBusinesses!!.size > 0) {
            //TODO add android resource string with placements
            if (item.connectedBusinesses!!.size == 1) {
                remark = "You have hired a business"
            } else {
                remark = "You have hired ${item.connectedBusinesses!!.size} businesses"
            }
        } else {
            remark = itemView.context.getString(R.string.business_count_empty_remark)
        }
        itemView.tvConnectedBusinessesRemark.setText(remark)
        businessThumbnailAdapter.submitList(item.connectedBusinesses)

        if (item.status == Job.Status.CLOSED) {
            itemView.ivMenu.visibility = View.GONE
            itemView.ivMenu.setOnClickListener(null)
        } else {
            itemView.ivMenu.visibility = View.VISIBLE
            itemView.ivMenu.setOnClickListener {
                val menu = PopupMenu(itemView.context, itemView.ivMenu)
                menu.inflate(R.menu.job_item)
                menu.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.close -> onCloseActionClicked(item)
                    }
                    true
                }
                menu.show()
            }
        }


    }
}
