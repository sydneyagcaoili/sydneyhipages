package au.com.hipages.features.jobs

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import au.com.hipages.R
import au.com.hipages.domain.models.Business
import au.com.hipages.domain.models.Job
import au.com.hipages.domain.models.toReadableDateString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_business_thumbnail.view.*
import kotlinx.android.synthetic.main.item_job.view.*

class BusinessThumbnailItemViewHolder(itemView : View) :
    RecyclerView.ViewHolder(itemView) {
    fun bind(item: Business) {
        Glide.with(itemView.context).load(item.thumbnailUrl).into(itemView.ivBusinessThumbnail);
    }
}
