package au.com.hipages.features.jobs


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import au.com.hipages.R
import au.com.hipages.bases.MvpFragment
import au.com.hipages.data.repositories.JobDataRepository
import au.com.hipages.domain.features.openjobs.OpenJobsPresenter
import au.com.hipages.domain.features.closedjobs.ClosedJobsView
import au.com.hipages.domain.models.Job
import au.com.hipages.domain.usecases.GetOpenJobsUseCase
import au.com.hipages.util.decoration.SpaceDecoration
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_open_jobs.*


class OpenJobsFragment : MvpFragment<ClosedJobsView, OpenJobsPresenter>(R.layout.fragment_open_jobs), ClosedJobsView {
    //TODO change to dependency injection
    private lateinit var adapter: JobAdapter

    override fun createPresenter() = OpenJobsPresenter(GetOpenJobsUseCase(JobDataRepository()))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareList(view)
        presenter.fetchOpenJobs()
    }


    private fun prepareList(view: View) {
        adapter = JobAdapter(onCloseActionClicked = {
            Toast.makeText(view.context, "Close button clicked for job: ${it.category}", Toast.LENGTH_LONG).show()
        })
        rvOpenJobs.layoutManager = LinearLayoutManager(view.context)
        rvOpenJobs.addItemDecoration(SpaceDecoration(rvOpenJobs.context, 8f))
        rvOpenJobs.adapter = adapter

        srlOpenJobs.setOnRefreshListener {
            presenter.fetchOpenJobs()
        }
    }

    override fun display(jobs: List<Job>) {
        adapter.submitList(jobs)
    }

    override fun handleError(e: Exception) {
        view?.let {
            Snackbar.make(it, "${e.message}", Snackbar.LENGTH_LONG).show()
        }
    }

    override fun displayLoadingState(show: Boolean) {
        srlOpenJobs.setRefreshing(show)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OpenJobsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
