package au.com.hipages.features.jobspager


import android.os.Bundle
import android.view.View
import au.com.hipages.R
import au.com.hipages.bases.ResFragment
import kotlinx.android.synthetic.main.fragment_jobs_pager.*


class JobTypesPagerFragment : ResFragment(R.layout.fragment_jobs_pager) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preparePager(view)
    }

    private fun preparePager(view : View) {
        vpJobTypes.adapter = JobTypeAdapter(view.context, childFragmentManager)
        tlJobTypes.setupWithViewPager(vpJobTypes)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            JobTypesPagerFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
