package au.com.hipages.features.main

import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import au.com.hipages.R
import au.com.hipages.bases.MvpActivity
import au.com.hipages.domain.features.main.MainPresenter
import au.com.hipages.domain.features.main.MainView
import au.com.hipages.features.jobspager.JobTypesPagerFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpActivity<MainView, MainPresenter>(), MainView {

    lateinit var toggle: ActionBarDrawerToggle

    //TODO apply Dagger
    override fun createPresenter() = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prepareToolbar()
        prepareDrawer()
        presenter.openInitialPage()
    }

    private fun prepareDrawer() {
        toggle = ActionBarDrawerToggle(
            this,
            dlMain,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        dlMain.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.logout -> finish()
            }
            dlMain.closeDrawer(GravityCompat.START)
            true
        }
    }

    private fun prepareToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowCustomEnabled(true)
    }

    override fun openJobsPage() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flContainer, JobTypesPagerFragment.newInstance())
            .commit()
    }

}
