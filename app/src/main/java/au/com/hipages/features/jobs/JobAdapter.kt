package au.com.hipages.features.jobs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import au.com.hipages.R
import au.com.hipages.domain.models.Job

class JobAdapter(var onCloseActionClicked: (job: Job) -> Unit? = {}) : ListAdapter<Job, JobItemViewHolder>(ItemDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobItemViewHolder {
        return JobItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_job, parent, false),
            onCloseActionClicked
        )
    }

    override fun onBindViewHolder(holder: JobItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class ItemDiffUtil : DiffUtil.ItemCallback<Job>() {
        override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }
}
