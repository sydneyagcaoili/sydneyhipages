package au.com.hipages.features.jobs


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import au.com.hipages.R
import au.com.hipages.bases.MvpFragment
import au.com.hipages.data.repositories.JobDataRepository
import au.com.hipages.domain.features.closedjobs.ClosedJobsPresenter
import au.com.hipages.domain.features.closedjobs.ClosedJobsView
import au.com.hipages.domain.models.Job
import au.com.hipages.domain.usecases.GetClosedJobsUseCase
import au.com.hipages.util.decoration.SpaceDecoration
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_closed_jobs.*


class ClosedJobsFragment : MvpFragment<ClosedJobsView, ClosedJobsPresenter>(R.layout.fragment_closed_jobs),
    ClosedJobsView {
    //TODO change to dependency injection
    private lateinit var adapter: JobAdapter

    override fun createPresenter() = ClosedJobsPresenter(GetClosedJobsUseCase(JobDataRepository()))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareList(view)
        presenter.fetchClosedJobs()
    }


    private fun prepareList(view: View) {
        adapter = JobAdapter()
        rvClosedJobs.layoutManager = LinearLayoutManager(view.context)
        rvClosedJobs.addItemDecoration(SpaceDecoration(rvClosedJobs.context, 8f))
        rvClosedJobs.adapter = adapter

        srlClosedJobs.setOnRefreshListener {
            presenter.fetchClosedJobs()
        }
    }

    override fun display(jobs: List<Job>) {
        adapter.submitList(jobs)
    }

    override fun handleError(e: Exception) {
        view?.let {
            Snackbar.make(it, "${e.message}", Snackbar.LENGTH_LONG).show()
        }
    }

    override fun displayLoadingState(show: Boolean) {
        srlClosedJobs.setRefreshing(show)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ClosedJobsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
