package au.com.hipages.features.jobs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import au.com.hipages.R
import au.com.hipages.domain.models.Business

class BusinessThumbnailAdapter : ListAdapter<Business, BusinessThumbnailItemViewHolder>(ItemDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessThumbnailItemViewHolder {
        return BusinessThumbnailItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_business_thumbnail,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BusinessThumbnailItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class ItemDiffUtil : DiffUtil.ItemCallback<Business>() {
        override fun areItemsTheSame(oldItem: Business, newItem: Business): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Business, newItem: Business): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }
}
