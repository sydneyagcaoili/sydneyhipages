package au.com.hipages.features.jobspager

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import au.com.hipages.R
import au.com.hipages.features.jobs.ClosedJobsFragment
import au.com.hipages.features.jobs.OpenJobsFragment

class JobTypeAdapter(var context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return OpenJobsFragment.newInstance()
            1 -> return ClosedJobsFragment.newInstance()
        }
        return Fragment()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return context.getString(R.string.open_jobs)
            1 -> return context.getString(R.string.closed_jobs)
        }
        return super.getPageTitle(position)
    }
}