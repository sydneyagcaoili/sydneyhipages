package com.cspreston.internal.presentation.util.extension

import android.location.Address

fun Address.getCompleteAddress() : String {
    val addressFragments: List<String> = (0..maxAddressLineIndex).map { getAddressLine(it) }
    return addressFragments.joinToString(separator = ",")
}
