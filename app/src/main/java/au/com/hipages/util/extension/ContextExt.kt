package au.com.hipages.util.extension

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.URLUtil
import android.widget.Toast
import java.io.IOException

/**
 * Get the screen's density
 */
fun Context.getDensity(): Float = resources.displayMetrics.density

/**
 * Convert dp to pixel
 */
fun Context.convertDpToPx(dp: Float): Int = (dp * resources.displayMetrics.density).toInt()

/**
 * Convert pixel to dp
 */
fun Context.convertPxToDp(px: Int): Float = px / resources.displayMetrics.density

/**
 * Hide soft keyboard
 */
fun Context.hideSoftKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Show soft keyboard
 */
fun Context.showSoftKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_IMPLICIT)
}

/**
 * Show toast
 */
fun Context.toast(msg: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, msg, duration).show()
}

/**
 * Get json from assets folder
 */
fun Context.getJsonFromAssets(filename: String): String? {
    return try {
        val inputStream = assets.open(filename)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        String(buffer, Charsets.UTF_8)
    } catch (e: IOException) {
        e.printStackTrace()
        null
    }
}

/**
 * Download file in "Downloads" folder
 */
fun Context.downloadFile(url: String, userAgent: String? = null, contentDisposition: String? = null,
    mimeType: String? = null) {
    val request = DownloadManager.Request(Uri.parse(url)).apply {
        val filename = URLUtil.guessFileName(url, contentDisposition, mimeType)
        setTitle(filename)
        setDescription("Downloading file...")
        setMimeType(mimeType)
        addRequestHeader("cookie", CookieManager.getInstance().getCookie(url))
        addRequestHeader("User-Agent", userAgent)
        allowScanningByMediaScanner()
        setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
    }

    val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
    dm.enqueue(request)
}