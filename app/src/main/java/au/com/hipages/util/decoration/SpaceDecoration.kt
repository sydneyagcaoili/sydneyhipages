package au.com.hipages.util.decoration

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import au.com.hipages.util.extension.convertDpToPx

class SpaceDecoration(context: Context, paddingLeft: Float = 0f, paddingTop: Float = 0f,
                      paddingRight: Float = 0f, paddingBottom: Float = 0f) : RecyclerView.ItemDecoration() {

    constructor(context: Context, padding: Float) : this(context, padding, padding, padding, padding)

    private val paddingLeftInPx: Int = context.convertDpToPx(paddingLeft)
    private val paddingTopInPx: Int = context.convertDpToPx(paddingTop)
    private val paddingRightInPx: Int = context.convertDpToPx(paddingRight)
    private val paddingBottomInPx: Int = context.convertDpToPx(paddingBottom)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = paddingTopInPx
        } else {
            outRect.top = 0
        }

        outRect.bottom = paddingBottomInPx
        outRect.left = paddingLeftInPx
        outRect.right = paddingRightInPx
    }
}
