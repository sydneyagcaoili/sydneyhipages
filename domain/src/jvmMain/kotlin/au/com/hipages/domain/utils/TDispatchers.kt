package au.com.hipages.domain.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

public actual object TDispatchers {

    public actual val Default: CoroutineDispatcher get() = Dispatchers.Default

    public actual val Main: CoroutineDispatcher get() = Dispatchers.Main

    public actual val Unconfined: CoroutineDispatcher get() = Dispatchers.Unconfined // Avoid freezing
}
