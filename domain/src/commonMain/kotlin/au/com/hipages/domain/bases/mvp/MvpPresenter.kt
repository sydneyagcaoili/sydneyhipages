package au.com.hipages.domain.bases.mvp

interface MvpPresenter<in V : MvpView> {
    fun attachView(view: V)
    fun detachView()
}