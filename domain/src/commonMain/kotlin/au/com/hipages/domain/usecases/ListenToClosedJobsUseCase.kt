package au.com.hipages.domain.usecases

import au.com.hipages.domain.models.Job
import au.com.hipages.domain.repositories.JobRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel

@ExperimentalCoroutinesApi
class ListenToClosedJobsUseCase constructor(val repository: JobRepository) {

    suspend fun execute(): BroadcastChannel<List<Job>> {
       return repository.listenToClosedJobs()
    }
}