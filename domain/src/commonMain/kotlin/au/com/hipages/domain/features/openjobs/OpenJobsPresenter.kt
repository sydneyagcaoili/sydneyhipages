package au.com.hipages.domain.features.openjobs

import au.com.hipages.domain.bases.mvp.BasePresenter
import au.com.hipages.domain.features.closedjobs.ClosedJobsView
import au.com.hipages.domain.usecases.GetOpenJobsUseCase
import au.com.hipages.domain.utils.TDispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class OpenJobsPresenter(private var getOpenJobsUseCase: GetOpenJobsUseCase) : BasePresenter<ClosedJobsView>() {

    fun fetchOpenJobs() {
        view?.displayLoadingState(true)
        GlobalScope.launch(TDispatchers.Default) {
            try {
                getOpenJobsUseCase.execute().consumeEach{
                    withContext(TDispatchers.Main) {
                        view?.displayLoadingState(false)
                        view?.display(it)
                    }
                }
            } catch (e: Exception) {
                withContext(TDispatchers.Main) {
                    //Log e
                    view?.displayLoadingState(false)
                    view?.handleError(e)
                }
            }
        }
    }
}