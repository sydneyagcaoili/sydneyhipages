package au.com.hipages.domain.usecases

import au.com.hipages.domain.models.Job
import au.com.hipages.domain.repositories.JobRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel

@ExperimentalCoroutinesApi
class GetOpenJobsUseCase constructor(val repository: JobRepository) {

    suspend fun execute() : ReceiveChannel<List<Job>> {
       return repository.fetchOpenJobs()
    }
}