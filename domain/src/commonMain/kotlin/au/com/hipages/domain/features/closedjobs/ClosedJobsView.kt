package au.com.hipages.domain.features.closedjobs

import au.com.hipages.domain.bases.mvp.MvpView
import au.com.hipages.domain.models.Job

interface ClosedJobsView : MvpView{
    fun handleError(e : Exception)
    fun display(jobs: List<Job>)
    fun displayLoadingState(show: Boolean)

}