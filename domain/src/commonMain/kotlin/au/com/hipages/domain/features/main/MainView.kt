package au.com.hipages.domain.features.main

import au.com.hipages.domain.bases.mvp.MvpView

interface MainView : MvpView{

    fun openJobsPage()
}