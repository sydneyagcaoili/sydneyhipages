package au.com.hipages.domain.features.openjobs

import au.com.hipages.domain.bases.mvp.MvpView
import au.com.hipages.domain.models.Job

interface OpenJobsView : MvpView{
    fun handleError(e : Exception)
    fun display(jobs: List<Job>)
    fun displayLoadingState(show: Boolean)

}