package au.com.hipages.domain.repositories

import au.com.hipages.domain.models.Job
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel

@ExperimentalCoroutinesApi
interface JobRepository {
    suspend fun fetchOpenJobs(): ReceiveChannel<List<Job>>
    suspend fun fetchClosedJobs(): ReceiveChannel<List<Job>>
    suspend fun listenToOpenJobs(): BroadcastChannel<List<Job>>
    suspend fun listenToClosedJobs(): BroadcastChannel<List<Job>>
}