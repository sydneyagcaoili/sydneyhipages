package au.com.hipages.domain.models

data class Business(
    val id: Long,
    val thumbnailUrl: String,
    val hired: Boolean){

}