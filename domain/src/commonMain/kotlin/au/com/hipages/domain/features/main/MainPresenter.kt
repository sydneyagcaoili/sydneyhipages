package au.com.hipages.domain.features.main

import au.com.hipages.domain.bases.mvp.BasePresenter
import au.com.hipages.domain.utils.TDispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainPresenter : BasePresenter<MainView>() {

    fun openInitialPage() {
        GlobalScope.launch(TDispatchers.Default) {
            //Some calculations if applicable
            withContext(TDispatchers.Main) {
                view?.openJobsPage()
            }
        }

    }
}