package au.com.hipages.domain.models

open class Job(
    val id: Long,
    val category: String,
    /*TODO improvise common multiplatform date class*/
    val postedDate: Date,
    val status: Status,
    val connectedBusinesses: List<Business>? = ArrayList()
) {

    companion object {
        fun determineStatus(stringValue: String): Status {
            return when {
                Status.IN_PROGRESS.stringValue.equals(stringValue, true) -> Status.IN_PROGRESS
                Status.CLOSED.stringValue.equals(stringValue, true) -> Status.CLOSED
                else -> Status.UNKNOWN
            }
        }
    }

    enum class Status(val stringValue: String) {
        IN_PROGRESS("In Progress"),
        CLOSED("Closed"),
        UNKNOWN("Unknown")
    }
}