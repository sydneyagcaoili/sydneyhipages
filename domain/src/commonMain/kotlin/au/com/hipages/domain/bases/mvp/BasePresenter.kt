package au.com.hipages.domain.bases.mvp



open class BasePresenter<V : MvpView>: MvpPresenter<V> {

    open var view: V? = null

    final override fun attachView(view: V) {
        if (!isViewAttached) {
            this.view = view
        }
    }

    override fun detachView() {
        view = null
    }


    private val isViewAttached: Boolean
        get() = view != null
}
