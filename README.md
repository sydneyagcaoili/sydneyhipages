Clean Architecture + Kotlin Multiplatform + MVP

Clean arch top modules:
*domain: The main logical representation of the app. This contains Domain models, Use cases, repository interfaces.
 The domain can be depended by multiple platforms like android, ios, and desktop apps

*data: An actual implementation of domain. This is where business logics are coded.
This contains Network models, Database models, Repositories, Mappers.
This is used by the apps to access data.

*app: Android module. Mainly consists of Android only stuffs and features.

Note: We may use the same domain and data resources for other platforms

KotlinMultiplatform libraries
Network: Ktor
Database: SQLDelight + platform drivers
Threading: Coroutines

Use case: A single app use case a feature may use
Repository: Sources of data
Mapper: Convert domain, data, and network models interchangeably

What's not yet included
*Dependency injection (needs plenty of time to setup)
*Actual Database implementation (Realm is very easy to setup, but we will use SQLDelight to support multiplatform on this project)


MVP + CleanArchitecture benefit: app modules may enjoy simply implementing MVP classes from domain.
Android developers can focus only android stuffs in his his own app module folder without any business logic codes implementing clean architecture.
Example: An android activity may implement the MvpView from the domain library and code UI stuffs on the implemented methods and presenter calls for actions.
This is the same for other platforms.